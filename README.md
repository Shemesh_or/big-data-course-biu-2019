# Big Data Course BIU - 2019
BigData project's README

Credits

This course uses Open Source components.
You can find the source material of their open source projects along with license information below.
We acknowledge and are grateful to these developers for their contributions to open source.

Data Analysis for Genomics http://genomicsclass.github.io/book/
Copyright (c) 2013 Rafael Irizarry and Michael Love
License (MIT) https://github.com/genomicsclass/labs/blob/master/LICENSE

Statistical Modeling: A Fresh Approach http://project-mosaic-books.com
Second Edition, Copyright (c) 2012, v2.0 Daniel Kaplan

Data Science Specialization https://github.com/DataScienceSpecialization/courses
© 2017 Coursera Inc. All rights reserved.
Contributors: Brian Caffo,Jeff Leek,Roger Peng,Nick Carchedi,Sean Kross License (CC-NC-SA) https://tldrlegal.com/license/creative-commons-attribution-noncommercial-sharealike-(cc-nc-sa)

Programming in R for Analytics http://www.contrib.andrew.cmu.edu/~achoulde/94842/
Contributors: Alexandra Chouldechova
License (CC BY-NC-SA 4.0) https://creativecommons.org/licenses/by-nc-sa/4.0/ 

